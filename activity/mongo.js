// s23 Activity

db.courses.insertMany([
	{name: "Basic HTML",
	 price: 100,
	 isActive: true,
	 description: "Lorem ipsum",
	 enrollee: []
	},

	{name: "Basic CSS",
	 price: 200,
	 isActive: true,
	 description: "Lorem ipsum",
	 enrollee: []
	},

	{name: "Basic Javascript",
	 price: 300,
	 isActive: true,
	 description: "Lorem ipsum",
	 enrollee: [
			 {
			 	userId: ObjectId("61fbca5cc7a47272c0e0619d") 
			 }
		]
	}
])
