/*Using MongoDB locally:

To use MongoDB on your local machine, you must first open a terminal and type the command "mongod" to run the MongoDB Community Server.

The server must always be running for the duration of your use of your local MongoDB interface/Mongo shelll.

Open a separate terminal and type "mongo" to open the Mongo Shell, which is a command line interface that allows us to issue MongoDB, commands

*/

show dbs

// Create a database.
//use <database name>

use myFirstDB

//the use command is only the first step in creating a database. Next, you must create a single record for the database to actually be created

//Create a single document

db.users.insertOne({name: "John Smith", age: 30, isActive: true})

//Get a list of all users

db.users.find()

//Create a second user

db.users.insertOne({name: "Jane Doe", age: 21, isActive: true})

//within each database is a number of collections, where multiple related data is stored (users, posts, products, etc)

//Create a collection

//If a collection does not exist yet, MongoDB creates the collection when yo u  first store data for that collection

db.products.insertMany([
	{name: "Product1", price: 200.50, stock: 100, description: "Lorem ipsum"},
	{name: "Product2", price: 333, stock: 25, description: "Lorem ipsum"}
	])

db.products.find({name: "Product2"})

db.users.updateOne(
	{_id: ObjectId("61fbcaaec7a47272c0e0619e")},
	{
		$set: {isActive: false}
	}
)